import pytest
from selenium.webdriver import Chrome

from lesson_20_selenium_framework.pages.dashboard.dashboard import Dashboard


@pytest.fixture(scope='session')
def driver():
    driver = Chrome("/home/co/PycharmProjects/hilel_lessons/lesson_19_selenium/drivers/chromedriver")
    driver.maximize_window()

    driver.get("https://www.olx.ua/uk/")
    # driver.add_cookie({"name": "auto", "value": "Ford"})
    # cookies = driver.get_cookies()
    # driver.execute_script("window.localStorage['car'] = 'Volvo'")
    # local_storage = driver.execute_script("return window.localStorage['car']")
    yield driver
    driver.quit()


@pytest.fixture(scope='session')
def dashboard(driver):
    yield Dashboard(driver)
