from selenium.webdriver.common.by import By

from lesson_20_selenium_framework.core.locator import Locator
from lesson_20_selenium_framework.pages.base_page import BasePage


class Product(BasePage):
    def __init__(self, driver):
        super().__init__(driver)

    def get_title(self, title):
        self.wait_for_element(Locator(By.XPATH, f"//title[text()='{title}']").to_tuple())
        return self.driver.title
