from typing import Tuple

from selenium.webdriver.common.by import By

from lesson_20_selenium_framework.pages.base_page import BasePage
from lesson_20_selenium_framework.pages.product import Product
from lesson_20_selenium_framework.core.locator import Locator


class ProductList(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self.__first_entry_locator: Locator = Locator(By.XPATH, "//div[@data-testid='listing-grid']/div[2]")

    def choose_product(self):
        self.click(self.__first_entry_locator)
        return Product(self.driver)
