from selenium.webdriver.common.by import By

from lesson_20_selenium_framework.core.locator import Locator


class DashboardLocatorCollection:
    def __init__(self):
        self.cookies_button = Locator(By.XPATH, "//button[@class='cookie-close abs cookiesBarClose']")

    @staticmethod
    def get_category(name):
        return Locator(By.XPATH, f"//span[text()='{name}']/ancestor::div[1]")

    @staticmethod
    def get_sub_category(name):
        return Locator(By.XPATH, f"//span[text()='{name}']/ancestor::a")
