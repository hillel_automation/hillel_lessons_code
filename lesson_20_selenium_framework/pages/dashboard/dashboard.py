from typing import Tuple

from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By

from lesson_20_selenium_framework.pages.base_page import BasePage
from lesson_20_selenium_framework.pages.dashboard.dashboard_locator_collection import DashboardLocatorCollection
from lesson_20_selenium_framework.pages.product_list import ProductList
from lesson_20_selenium_framework.core.locator import Locator


class Dashboard(BasePage):

    def __init__(self, driver: Chrome):
        super().__init__(driver)
        self.__locator_collection = DashboardLocatorCollection()

    def choose_category(self, name: str):
        # category_locator: Locator = self.__locator_collection.get_category(name)
        # cookies_locator = self.__locator_collection.cookies_button
        self.click(self.__locator_collection.cookies_button)
        self.click(self.__locator_collection.get_category(name))

    def choose_sub_category(self, name) -> ProductList:
        self.click(self.__locator_collection.get_sub_category(name))
        return ProductList(self.driver)
