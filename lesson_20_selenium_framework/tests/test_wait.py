from time import sleep

from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from  selenium.webdriver.common.action_chains import ActionChains


def test_1():
    driver = Chrome("../drivers/chromedriver")
    # driver.implicitly_wait(5)
    web_driver_wait = WebDriverWait(driver, 10)
    print(driver.get_cookies())
    driver.get("https://www.google.com/")
    search_locator = "//input[@title='Пошук']"
    first_finded_element = "//ul[@role='listbox']/li[2]"

    # search_input = web_driver_wait.until(EC.presence_of_element_located((By.XPATH, search_locator)))
    # search_input.send_keys("Hello")
    #
    # second_result: WebElement = web_driver_wait.until(EC.presence_of_element_located((By.XPATH, first_finded_element)))
    # second_result.click()

    driver.quit()


def test_2():
    driver = Chrome("../drivers/chromedriver")
    # driver.implicitly_wait(5)
    web_driver_wait = WebDriverWait(driver, 10)
    driver.get("https://ebook.online-convert.com/convert-to-mobi")
    element: WebElement = web_driver_wait.until(EC.presence_of_element_located((By.XPATH, "//input[@type='file']")))
    element.send_keys("/home/co/PycharmProjects/hilel_lessons/lesson_20_selenium_framework/tests/text.txt")

    sleep(10)
    driver.quit()


def test_3():
    driver = Chrome("../drivers/chromedriver")
    # driver.implicitly_wait(5)
    web_driver_wait = WebDriverWait(driver, 10)
    driver.get("https://demo.guru99.com/test/drag_drop.html")
    element_from: WebElement = web_driver_wait.until(
        EC.presence_of_element_located((By.XPATH, "//ul/li[@id='fourth']")))
    element_to: WebElement = web_driver_wait.until(
        EC.presence_of_element_located((By.XPATH, "//ol[@id='amt7']")))
    action = ActionChains(driver)
    sleep(2)
    action.drag_and_drop(element_from, element_to).perform()

    sleep(10)
    driver.quit()
