pipeline {
    agent {
        label "docker"
    }

    environment {
        GIT_REPO_LINK = "https://gitlab.com/hillel_automation/hillel_lessons_code.git"
    }

    stages {
        stage('Build') {
            steps {
               sh """
                git clone $GIT_REPO_LINK
                pip3 install pytest-html
                echo "Build successful"
               """
            }
        }

        stage('Test'){
            steps{
                sh"""
                    cd hillel_lessons_code/lesson_17_pytest/func_way
                    pytest . --html=report.html --self-contained-html
                    ls
                """
            }
        }
    }
    post {
        always {
            sh """
                mkdir -p $WORKSPACE/$BUILD_NUMBER
                cp hillel_lessons_code/lesson_17_pytest/func_way/report.html $WORKSPACE/$BUILD_NUMBER/
                #cp -r hillel_lessons_code/lesson_17_pytest/func_way/assets/ $WORKSPACE/$BUILD_NUMBER/

            """
            archiveArtifacts artifacts: "$BUILD_NUMBER/report.html"
//             archiveArtifacts artifacts: "$BUILD_NUMBER/**/*.*"
            publishHTML (target : [allowMissing: false,
                 alwaysLinkToLastBuild: true,
                 keepAll: true,
                 reportDir: "$BUILD_NUMBER",
                 reportFiles: 'report.html',
                 reportName: 'My Reports',
                 reportTitles: 'The Report'])
        }
    }
}
