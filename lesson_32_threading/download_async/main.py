from threading import Thread
import requests
from time import time


def download(link, file_path):
    responce = requests.get(link, stream=True)
    with open(file_path, "wb") as file:
        for chunk in responce.iter_content(1024):
            if chunk:
                file.write(chunk)


def create_new_download(link, file_path):
    thread = Thread(target=download, args=(link, file_path))
    thread.start()


start = time()

# for i in range(10):
#     create_new_download("https://stackoverflow.com/users/flair/2374517.png", f"file{i}")

for i in range(10):
    download("https://stackoverflow.com/users/flair/2374517.png", f"file{i}")

end = time()

print(f"Files downloaded in {end - start} seconds")
