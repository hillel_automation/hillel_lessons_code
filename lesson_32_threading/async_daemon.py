from threading import Thread
from time import sleep

def job1():
    for _ in range(10):
        print("Child job1")
        sleep(0.5)

def job2():
    for _ in range(5):
        print("Child job2")
        sleep(1)

thread = Thread(target=job1, daemon=True)
thread.start()
thread2 = Thread(target=job2, daemon=True)
thread2.start()

for _ in range(5):
    print("Main job")
    sleep(0.25)

print("Finish")