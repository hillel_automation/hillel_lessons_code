from time import sleep
from threading import Thread


def job():
    for i in range(2):
        print(f"Child job iteration {i}")
        sleep(1)


thread = Thread(target=job)
print(thread.daemon)
thread.daemon = True
print(thread.daemon)
thread.start()

for i in range(2):
    print("Main job")
    sleep(0.5)
