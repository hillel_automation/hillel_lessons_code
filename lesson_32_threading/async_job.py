from time import sleep
from threading import Thread


def job1():
    for _ in range(5):
        print("Child job 1")
        sleep(1)

def job2():
    for _ in range(5):
        print("Child job 2")
        sleep(0.5)

thread1 = Thread(target=job1)
thread2 = Thread(target=job2)
thread1.start()
thread2.start()

for _ in range(5):
    print("Main thread")
    sleep(0.25)

print("Finish")