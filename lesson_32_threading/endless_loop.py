from  time import sleep
from threading import Thread

def loop():
    i = 0
    while True:
        print(f"Loop iteration{i}")
        i+=1
        sleep(0.5)


thread = Thread(target=loop, daemon=True)

thread.start()


for i in range(5):
    print(f"Main loop {i}")
    sleep(0.5)

print("Finish")



