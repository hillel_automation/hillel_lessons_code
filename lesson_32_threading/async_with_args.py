from time import sleep
from threading import Thread


#
# def job1(iterations, sleep_time, job_name):
#     for i in range(iterations):
#         print(f"{job_name}, iteration {i}")
#         sleep(sleep_time)
#
# thread1 = Thread(target=job1, args=(5, 0.5, "Child job 1"))
# thread2 = Thread(target=job1, args=(3, 0.25, "Child job 2"))
# thread1.start()
# thread2.start()
#
# for i in range(5):
#     print(f"Main job {i}")
#     sleep(0.2)


def job1(iterations, sleep_time, job_name):
    for i in range(iterations):
        print(f"{job_name}, iteration {i}")
        sleep(sleep_time)


thread1 = Thread(target=job1, kwargs={"iterations": 5, "sleep_time": 0.5, "job_name": "Child job 1"})
thread2 = Thread(target=job1, kwargs={"iterations": 3, "sleep_time": 0.25, "job_name": "Child job 2"})
thread1.start()
thread2.start()
thread2.run()

for i in range(5):
    print(f"Main job {i}")
    sleep(0.2)
