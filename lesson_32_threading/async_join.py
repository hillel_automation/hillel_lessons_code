from time import sleep
from threading import Thread

def job1():
    for i in range(5):
        print(f"Child job 1 iteration {i}")
        sleep(0.5)


def job2():
    for i in range(5):
        print(f"Child job 2 iteration {i}")
        sleep(1)


thread1 = Thread(target=job1)
print(f"Is thread 1 alive {thread1.is_alive()}")
# thread2 = Thread(target=job2)
thread1.start()
print(f"Is thread 1 alive {thread1.is_alive()}")
thread1.join()
print(f"Is thread 1 alive {thread1.is_alive()}")
# thread2.start()


for i in range(5):
    print(f"Main job {i}")
    sleep(0.25)

print("Finish")




