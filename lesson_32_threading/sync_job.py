from time import sleep

def do_job():
    for i in range(10):
        print("Child job")
        sleep(0.5)
do_job()

for i in range(5):
    print("Main job")
    sleep(1)
