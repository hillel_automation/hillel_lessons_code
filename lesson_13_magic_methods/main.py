from lesson_13_magic_methods.donkey import Donkey
from lesson_13_magic_methods.horse import Horse
from lesson_13_magic_methods.mule import Mule
from lesson_13_magic_methods.traffic_light import TrafficLight

if __name__ == "__main__":
    # trafic_light = TrafficLight("WH")
    # str_traffic_light = str(trafic_light)
    # print(trafic_light)
    # print(trafic_light._TrafficLight__address)
    # print(trafic_light.color)
    horse = Horse(speed=5, age=2)
    donkey = Donkey(strength=7, age=2)
    # mule = horse + donkey
    # print(mule)
    # new_mule = donkey + horse
    # print(new_mule)
    # horse += donkey
    # print(horse.age)
    # print(bool(horse))
    # print(float(horse))
    print(horse == donkey)
    print(horse <= donkey)
