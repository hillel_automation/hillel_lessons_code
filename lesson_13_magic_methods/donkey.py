# from lesson_13_magic_methods.horse import Horse
from lesson_13_magic_methods.mule import Mule


class Donkey:
    def __init__(self, strength, age):
        self.strength = strength
        self.age = age

    def __str__(self):
        return f"{self.__class__.__name__}:\n {{\n\t strength: {self.strength}\n\t age: {self.age}\n }}"

    # def __add__(self, other) -> Mule:
    #     created_mule = Mule(strength=self.strength, speed=other.speed)
    #     return created_mule


