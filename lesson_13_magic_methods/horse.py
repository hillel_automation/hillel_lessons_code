from lesson_13_magic_methods.donkey import Donkey
from lesson_13_magic_methods.mule import Mule


class Horse:
    def __init__(self, speed, age):
        self.speed = speed
        self.age = age

    def __str__(self):
        return f"{self.__class__.__name__}:\n {{\n\t speed: {self.speed}\n\t age: {self.age}\n }}"

    def __add__(self, other: Donkey) -> Mule:
        created_mule = Mule(speed=self.speed, strength=other.strength)
        return created_mule

    def __radd__(self, other: Donkey) -> Mule:
        created_mule = Mule(speed=self.speed, strength=other.strength)
        return created_mule

    def __iadd__(self, other: Donkey):
        self.age += other.age
        return self

    # def __bool__(self):
    #     return False

    def __float__(self):
        return float(self.age)

    def __eq__(self, other):
        return self.age == other.age

    def __le__(self, other):

    def __ge__(self, other):

