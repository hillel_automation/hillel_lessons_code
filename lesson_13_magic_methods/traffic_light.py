class TrafficLight:

    def __init__(self, address):
        self.__color = "GREEN"
        self.__address = address

    def __str__(self):
        return f"{self.__class__.__name__}:\n {{\n\t color: {self.__color}\n }}"

    def __repr__(self):
        return f"REPR: New traffic light with color {self.__color}"

    def __getattr__(self, item):
        return f"Class {self.__class__.__name__} has no attribute {item}"

    @property
    def color(self):
        return self.__color

    def __getattribute__(self, item: str):
        # if item.startswith("_TrafficLight__"):
        #     return "You can not get private fields little hacker"
        return super().__getattribute__(item)