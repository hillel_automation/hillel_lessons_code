from lesson_33_async_multiprocess.async_lock.employee import Employee
from threading import Thread

if __name__ == "__main__":
    employee = Employee()

    thread1 = Thread(target=employee.increase_rate)
    thread2 = Thread(target=employee.increase_rate)
    thread1.start()
    thread2.start()
    thread1.join()
    thread2.join()

    print(employee.rate)
