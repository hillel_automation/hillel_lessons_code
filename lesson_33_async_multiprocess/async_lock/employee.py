from time import sleep
from threading import Lock, RLock, Semaphore


class Employee:

    def __init__(self):
        self.rate = 0
        self.__lock = Semaphore(2)

    def increase_rate(self):
        self.__lock.acquire()
        print("Hello")
        self.__lock.acquire()
        local_value = self.rate
        local_value += 10
        sleep(0.1)
        self.rate = local_value
        self.__lock.release()
        print("Goodbye")
        self.__lock.release()

