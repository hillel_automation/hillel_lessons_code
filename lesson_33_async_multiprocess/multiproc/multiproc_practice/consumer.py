from os import listdir
from time import time
from concurrent.futures import ThreadPoolExecutor
from multiprocessing import Pool

report_path = "/home/co/reports"
names = listdir(report_path)


def read_from_report(file_path: str):
    with open(file_path) as file:
        return file.read()


if __name__ == "__main__":
    start = time()
    # sync way Spent time 9.145684957504272 seconds
    # for name in names:
    #     read_from_report(f"{report_path}/{name}")

    # async Spent time 13.830549001693726 seconds
    # with ThreadPoolExecutor(20) as executor:
    #     for name in names:
    #         executor.submit(read_from_report, args=(f"{report_path}/{name}",))

    # parallel Spent time 1.2480380535125732 seconds
    with Pool(12) as pool:
        pool.map(read_from_report, (f"{report_path}/{name}" for name in names))

    end = time()
    print(f"Spent time {end - start} seconds")
