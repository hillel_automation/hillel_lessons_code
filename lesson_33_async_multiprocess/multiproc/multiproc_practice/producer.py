from json import dumps

for i in range(100000):
    name = f"report_{i}.json"
    report_content = dumps({
        "name": f"test_{i}",
        "result": "Pass"
    })

    with open(f"/home/co/reports/{name}", "w") as report:
        report.write(report_content)