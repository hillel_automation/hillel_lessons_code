from multiprocessing import Process
from time import sleep, time


def func():
    print("Start")
    sleep(2)
    print("Finish")


if __name__ == "__main__":
    processes = []

    start = time()

    for i in range(5):
        process = Process(target=func)
        process.start()
        processes.append(process)

    for process in processes:
        process.join()

    end = time()

    print(f"Spent time {end - start} seconds")
