from threading import Thread
from time import sleep, time


def func():
    print("Start")
    sleep(2)
    print("Finish")

threads = []

start = time()

for i in range(5):
    thread = Thread(target=func)
    thread.start()
    threads.append(thread)

for thread in threads:
    thread.join()

end = time()

print(f"Spent time {end-start} seconds")
