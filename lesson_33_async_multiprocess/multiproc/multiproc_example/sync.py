from time import sleep, time


def func1():
    print("Start")
    sleep(2)
    print("Finish")


start = time()

for i in range(5):
    func1()

end = time()

print(f"Spent time {end-start} seconds")