from time import sleep

from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.common.action_chains import ActionChains


def test_1():
    driver = Chrome("drivers/chromedriver")
    driver.get("https://www.google.com/")
    search_locator = "//input[@title='Пошук']"
    first_finded_element = "//ul[@role='listbox']/li[2]"
    desire_video = "//div[@role='heading']/div[1]"
    search_input: WebElement = driver.find_element(By.XPATH, search_locator)
    search_input.send_keys("Hello")
    sleep(1)
    second_result: WebElement = driver.find_element(By.XPATH, first_finded_element)
    second_result.click()
    sleep(2)
    video_element: WebElement = driver.find_element(By.XPATH, desire_video)
    video_element.click()

    sleep(1)
    driver.quit()


def test_2():
    driver = Chrome("drivers/chromedriver")
    driver.maximize_window()
    driver.get("https://www.google.com/")
    search_locator = "//input[@title='Пошук']"
    search_input: WebElement = driver.find_element(By.XPATH, search_locator)
    search_input.send_keys("Hello")
    action = ActionChains(driver)
    action.key_down(Keys.CONTROL).send_keys("a").key_up(Keys.CONTROL).perform()
    sleep(2)


def test_3():
    driver = Chrome("drivers/chromedriver")
    # driver.maximize_window()
    driver.get("https://www.google.com/")
    search_locator = "//input[@title='Пошук']"
    search_input: WebElement = driver.find_element(By.XPATH, search_locator)
    # search_input.send_keys("Hello")
    # search_input.send_keys(Keys.ENTER)
    # driver.execute_script("alert('Hello');")
    # driver.save_screenshot("screen.jpg")

    sleep(2)
