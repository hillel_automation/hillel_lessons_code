# file_obj = open("file.txt", "r", encoding="utf-8")
# print(file_obj.read())
#
# file_obj.close()
# import os
# path = os.path.dirname(os.path.abspath(__file__))
# print(path)
#
# with open(f"{path}/text_dir/file.txt", "r") as file_object:
#     print(file_object.read())

class File:
    def __init__(self, file_name, mode):
        self.file_name = file_name
        self.mode = mode
        self.__io_wrapper = None

    def read(self):
        return self.__io_wrapper.read()

    def __enter__(self):
        self.__io_wrapper = open(self.file_name, self.mode)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__io_wrapper.close()

if __name__ == "__main__":
    with File("text_dir/file.txt", "r") as file_obj:
        print(file_obj.read())

