numbers= [1,2,3,5]
# for number in numbers:
#     print(number)
iterator = iter(numbers)


class CustomIterator:
    def __init__(self, sequence):
        self.sequence = sequence
        self.position = 0

    def __iter__(self):
        return self

    def __next__(self):
        if len(self.sequence) > self.position:
            item = self.sequence[self.position]
            self.position+=1
            return item
        else:
            raise StopIteration

if __name__ == "__main__":
    custom_iter = CustomIterator([1,2,4,5])
    print(next(iter(custom_iter)))