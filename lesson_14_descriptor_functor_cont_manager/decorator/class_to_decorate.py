from lesson_14_descriptor_functor_cont_manager.decorator.decorator_class import Hobby


@Hobby("Politics")
class Human:
    def __init__(self, name, age):
        self.__name = name
        self.__age = age

    @property
    def name(self):
        return self.__name


@Hobby("Barking")
class Dog:
    pass

if __name__ == "__main__":
    joe = Human("Joe", 80)
    print(joe.name)
    print(joe.hobby)
    dog = Dog()
    print(dog.hobby)
    dog.age =2
