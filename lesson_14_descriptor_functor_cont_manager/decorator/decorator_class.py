class Hobby:
    def __init__(self, name: str):
        self.name = name

    def __call__(self, _class):
        _class.hobby = self.name
        return _class
