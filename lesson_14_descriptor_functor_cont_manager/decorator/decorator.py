from typing import List, Callable
#
# class Pow:
#     def __init__(self, func: Callable):
#         self.func = func
#
#     def __call__(self, a, b):
#         result = self.func(a, b)
#         return result **2
#
# @Pow
# def multiple(a,b):
#     return a*b
#
# print(multiple(2,2))



class Pow:
    def __init__(self, pow):
        self.__pow = pow

    def __call__(self, func):
        def inner(a, b):
            return func(a, b) ** self.__pow
        return inner

@Pow(2)
def multiple(a,b):
    return a*b


print(multiple(2,2))

