class Phone:
    def __init__(self, model, space):
        self.model = model
        self.space = space

    def __call__(self, number):
        return f"My phone number is {number}"

if __name__ == "__main__":
    phone = Phone("x12", "100gb")
    print(phone("123456"))

