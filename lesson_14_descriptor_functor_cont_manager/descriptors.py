class Column:

    def __get__(self, instance, value):
        return self.__value

    def __set__(self, instance, value):
        self.__value = value

    def __del__(self):
        print("Del method")

class Table:
    id = Column()

if __name__ == "__main__":
    table = Table()
    table.id = 1
    print(table.id)