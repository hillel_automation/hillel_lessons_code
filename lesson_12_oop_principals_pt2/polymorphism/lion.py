from lesson_12_oop_principals_pt2.polymorphism.cat import Cat


class Lion(Cat):
    def __init__(self):
        self.legs = 4
        self.tail = "Long"
        self.color = "Yellow"

    def make_noise(self):
        print("Rrrrr")
