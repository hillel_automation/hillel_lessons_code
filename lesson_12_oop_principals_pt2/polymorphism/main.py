from lesson_12_oop_principals_pt2.polymorphism.home_cat import HomeCat
from lesson_12_oop_principals_pt2.polymorphism.lion import Lion

if __name__ == "__main__":
    lion = Lion()
    homecat = HomeCat()
    # lion.make_noise()
    print(homecat.make_noise("Meow"))