from lesson_12_oop_principals_pt2.polymorphism.cat import Cat


class HomeCat(Cat):

    def __init__(self):
        self.legs = 4
        self.tail = "Short"
        self.color = "Grey"

    def make_noise(self, nose):
        return nose
