from abc import ABC, abstractmethod


class ITransport(ABC):

    @abstractmethod
    def start_engine(self):
        """Declare start engine behavior for child classes"""
        pass

    @abstractmethod
    def stop_engine(self):
        """Declare start engine behavior for child classes"""
        pass

    @abstractmethod
    def move(self, direction: str, distance: int):
        pass

