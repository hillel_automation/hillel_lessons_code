from lesson_12_oop_principals_pt2.interfaces.helicopter import Helicopter

if __name__ == "__main__":
    helicopter = Helicopter()
    print(helicopter.get_coordinates())
    print(helicopter.is_on_land())
    helicopter.move("up", 10)
    print(helicopter.get_coordinates())
    print(helicopter.is_on_land())
    helicopter.move("forward", 100)
    print(helicopter.get_coordinates())
