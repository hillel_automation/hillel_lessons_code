from abc import ABC, abstractmethod


class IFlyable(ABC):

    @abstractmethod
    def fly(self):
        """Declare fly behavior for child classes"""
        pass

    @abstractmethod
    def norsdive(self):
        """Declare norsdive behavior for child classes"""
        pass