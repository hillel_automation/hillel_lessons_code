from lesson_12_oop_principals_pt2.interfaces.iflyable import IFlyable
from lesson_12_oop_principals_pt2.interfaces.itransport import ITransport


class Helicopter(IFlyable, ITransport):

    def __init__(self):
        self.doors = 2
        self.blades = 4
        self.__engine_status = True
        self.__is_on_land = True
        self.__x = 0
        self.__y = 0
        self.__z = 0

    def fly(self):
        pass

    def norsdive(self):
        pass

    def start_engine(self) -> None:
        """Start engine oon helicopter"""
        self.__engine_status = True

    def stop_engine(self) -> None:
        """Stop engine on helicopter"""
        if self.__is_on_land:
            self.__engine_status = False

    @property
    def engine_status(self) -> bool:
        """
        Get helicopter engine status

        Returns:
            Status of the engine of the helicopter
        """
        return self.__engine_status

    def move(self, direction: str, distance: int) -> None:
        """
        Set coordinates where to move helicopter
        Args:
            direction(str): direction to move helicopter
            distance(int): value to move helicopter to the direction
        """
        if direction == "up":
            self.__z += distance
            self.__is_on_land = False
        elif direction == "down":
            if distance > self.__z:
                self.__z = 0
                self.__is_on_land = True
            else:
                self.__z -= distance
        elif direction == "right":
            self.__y += distance
        elif direction == "left":
            self.__y -= distance
        elif direction == "forward":
            self.__x += distance
        elif direction == "back":
            self.__x -= distance
        else:
            print("Incorrect direction")

    def get_coordinates(self) -> tuple:
        """
        Get coordinates of the helicopter

        Returns:
            Tuple of coordinates x, y, z
        """
        return self.__x, self.__y, self.__z

    def is_on_land(self) -> bool:
        """
        Get is on land status

        Returns:
            Is on land status
        """
        return self.__is_on_land
