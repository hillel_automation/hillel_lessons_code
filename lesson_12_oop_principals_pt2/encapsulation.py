class Human:
    """Class describes behavior of object Human"""

    def __init__(self, name: str, age: int):
        self.__name = name
        self.__age = age
        self.address = "WH"

    @property
    def name(self) -> str:
        new_name = f"My name is {self.__name}"
        return new_name

    def get_age(self) -> int:
        """
        Get human age

        Returns:
            self.__age: Age of current human
        """
        return self.__age

    @name.setter
    def name(self, new_name: str) -> None:
        """
        Change human name

        Args:
            new_name(str): New name for human

        """
        self.__name = new_name

    @name.deleter
    def name(self):
        self.__name = ""


if __name__ == "__main__":
    human = Human("Joe", 84)
    print(human.name)
    print(f"My name is {human.name}")
    print(human.get_age())


    human.name = "Jeff"
    print(human.name)

    del human.name
    print(human.name)
