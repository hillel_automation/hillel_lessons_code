class Human:
    def __init__(self, name=None, age=None):
        self._name = name
        self.__age = age
        self.salary = 100
        self.name = 10

    def get_name(self):
        return self._name

    def get_age(self):
        return self.__age


joe = Human("Joe", 84)
bob = Human("Bob", 12)
joe.name = 10
print(joe.get_name())
print(joe.get_age())





