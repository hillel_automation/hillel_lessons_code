class Human:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def get_name(self):
        return self.name


joe = Human("Joe", 84)
maria = Human("Maria", 30)
print(joe.get_name())

# class Human:
#     age = 84
#     name = "Joe"
#     addresses = []
#
#
#
# joe = Human()
# maria = Human()
# print(joe.age)
# joe.age = 50
# print(joe.age)
# print(maria.age)
# joe.addresses.append("WH")
# print(joe.addresses)
# print(maria.addresses)
