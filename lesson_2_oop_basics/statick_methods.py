class Human:

    addresses = []

    def __init__(self, name: str, age: int):
        self.name = name
        self.age = age

    def get_name(self):
        return self.name

    @classmethod
    def add_addresses(cls, address):
        return cls.addresses.append(address)

    @staticmethod
    def get_hello(name):
        return f"Hello {name}"


joe: Human = Human("Joe", 30)
maria = Human("Maria", 20)
joe.age = 84
print(maria.addresses)
print(joe.addresses)
print(type(joe))
Human.add_addresses("WH")
#
print(joe.addresses)
print(maria.addresses)
Human.add_addresses("G")
print(joe.addresses)
# print(joe.get_hello("Nathan"))
# print(Human.get_hello("Ben"))
name: str = "Joe"
print(name)

