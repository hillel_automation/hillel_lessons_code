"""Func for import"""
name = "Joe"
age = 84
salary = 100500

people = [
    {"name": "Joe",
     "age": 84,
     "salary": 100500},
    {"name": "Macron",
     "age": 45,
     "salary": 100500}
]


def get_salary(index):
    return people[index]["salary"]


if __name__ == "__main__":
    print(get_salary(0))
