import pytest


@pytest.mark.parametrize("gender,expected_gender", [
    ("male", "male"),
    ("female", "female"),
],
     ids=["Run test for male", "Run test for female"])
def test_human_gender(human, gender, expected_gender):
    human.change_gender(gender)
    assert human.gender == expected_gender


def test_wrong_gender(human):
    with pytest.raises(Exception):
        human.change_gender("")


def test_save_state(human, monkeypatch):
    monkeypatch.setattr(
        human, "_Human__gender", "male"
    )
    assert human.gender == "male"


def test_gender(human):
    assert human.gender == "female"
