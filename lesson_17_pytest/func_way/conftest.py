import pytest

from lesson_17_pytest.action import Action
from lesson_17_pytest.human import Human


@pytest.fixture(scope="module")
def action() -> Action:
    yield Action("playing")


@pytest.fixture(scope="module")
def human(action) -> Human:
    # print('Precondition')
    yield Human("Bob", 23, "male", action)
    # print('Teardown')
