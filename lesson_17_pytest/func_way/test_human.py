import sys

import pytest


def test_change_gender(human):
    human.change_gender("female")
    assert human.gender == "female"


# def test_gender(human):
#     assert human.gender == "female"


def test_grow(human):
    human.grow()
    assert human.age == 24


@pytest.mark.xfail(sys.platform == "linux", reason="Fail if os is linux")
def test_xfail(human):
    assert human.age == 26


@pytest.mark.skip
def test_skip(human):
    assert human.age == 26


@pytest.mark.skipif(sys.platform == "linux", reason="Skip if os is linux")
def test_skip_if(human):
    assert human.age == 24


@pytest.mark.smoke
def test_smoke(human):
    assert human.name == "Bob"


@pytest.mark.smoke
@pytest.mark.general
def test_general_name(human):
    assert human.name == "Bob"

