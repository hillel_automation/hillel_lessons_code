from lesson_17_pytest.action import Action


class Human:
    def __init__(self, name: str, age: int, gender: str, action: Action):
        self.__name = name
        self.__age = age
        self.__gender = gender
        self.__action = action

    def grow(self):
        self.__age += 1

    def change_gender(self, gender: str) -> None:
        if gender not in ["male", "female"]:
            raise Exception("Incorrect gender")
        self.__gender = gender

    @property
    def name(self) -> str:
        return self.__name

    @property
    def age(self) -> int:
        return self.__age

    @property
    def gender(self):
        return self.__gender

# human = Human("Bob", 32, "male")
# print(human.age)
