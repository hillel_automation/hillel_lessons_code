from lesson_17_pytest.human import Human


class TestHuman:

    def setup_class(self):
        self.human = Human("Bob", 32, 'male')

    def setup(self):
        print("Setup for each test")

    def test_age(self):
        self.human.grow()
        assert self.human.age == 33, f"Incorrect human age expected"

    def test_change_gender(self):
        self.human.change_gender("female")
        assert self.human.gender == "female"

    def teardown(self):
        print("Teardown after each test")

    def teardown_class(self):
        print("Teardown")






