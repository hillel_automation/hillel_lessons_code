x, y, z = 1, 2, 3
print(f"x={x}")
(x, y, z) = (4, 5, 6)
print(f"y={y}")
x, y = [8, 9]
print(f"new_x={x}")
x,y = 'hi'
print(x)
print(y)


list_ = [1, 2, 3]
del list_[1]
print(len(list_[1:]))
