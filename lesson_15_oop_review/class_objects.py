from lesson_15_back_to_the_root.class_example import Dog

if __name__ == "__main__":
    rex = Dog(4, True, ["black", "brown"], "short", "good")
    dog_health = rex.health
    print(dog_health)
    rex.health = "bad"
    print(rex.health)
    print(rex.make_voise())
