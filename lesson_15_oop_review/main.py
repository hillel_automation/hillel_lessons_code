# a = 2, 3
# b = a*5+1
# print(b)

number = 5.3472559
result = "{:.3f}".format(number)
print(result)

list_ = [1, 2, 3]
print(list_ is list_[:])
print(list_ == list_[:])