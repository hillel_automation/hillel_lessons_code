from abc import ABC, abstractmethod


class Animal(ABC):
    def __init__(self, fur, tail):
        self._fur = fur
        self._tail = tail
        self.something = None
        self.__color = None
        self.__health = None

    @abstractmethod
    def make_voice(self):
        """Method to describe voice of child classes"""
        pass

    def eating(self):
        return "Food have eaten"
