from typing import List

from lesson_15_back_to_the_root.animl import Animal


class Dog(Animal):
    def __init__(self, fur, tail, color, legs, health):
        super().__init__(tail=tail, fur=fur)
        self.__color = color
        self.__legs = legs
        self.__health = health
        self.__is_alive = True
        self.hungry = False
        self.fullness = 100

    @property
    def health(self):
        return self.__health

    @health.setter
    def health(self, health):
        if type(health) == str:
            self.__health = health
        else:
            raise AttributeError

    @property
    def color(self) -> List[str]:
        """
        Get list of dogs color
        Returns:
            self.color(List): list of main colors of dogs fur
        """
        return self.__color

    def make_voice(self) -> str:
        """
        Get dog voice

        Returns:
            str: Dog voise
        """
        self.fullness -= 5
        return "Bark"

    def eating(self) -> None:
        """
        Ful dog food capacity
        """
        self.fullness = 100
        self.hungry = False

    def playing(self) -> str:
        """
        Get dog action playing, that reduce dog fullness
        Returns:
            str: Dog action playing

        """
        self.fullness -= 20
        if self.fullness < 50:
            self.hungry = True
        return "Playing"

