from typing import List, Dict

class Dog:
    """Class that describe Dog object"""

    def __init__(self, legs: int, tail: bool, color: List[str], fur: str, health: str):
        """

        Args:
            legs: number of legs
            tail: is tail exist
            color: color of fur
            fur: fur type
        """
        self.__legs = legs
        self.__tail = tail
        self.__color = color
        self.__fur = fur
        self.__is_alive = True
        self.hungry = False
        self.fullness = 100
        self.__health = health

    @property
    def health(self):
        return self.__health

    @health.setter
    def health(self, health):
        self.__health = health

    @property
    def color(self) -> List[str]:
        """
        Get list of dogs color
        Returns:
            self.color(List): list of main colors of dogs fur
        """
        return self.__color

    @staticmethod
    def make_voise() -> str:
        """
        Get dog noise

        Returns:
            str: Dog voise
        """
        return "Bark"

    def eating(self) -> None:
        """
        Ful dog food capacity
        """
        self.fullness = 100
        self.hungry = False

    def playing(self) -> str:
        """
        Get dog action playing, that reduce dog fullness
        Returns:
            str: Dog action playing

        """
        self.fullness -= 20
        if self.fullness < 50:
            self.hungry = True
        return "Playing"








