from lesson_15_back_to_the_root.inherit_from_dog import Dog

if __name__ == "__main__":
    rex = Dog(4, True, ["black", "brown"], "short", "good")
    bobik = Dog(4, True, ["grey"], "long", "good")
    print(rex.health)
    rex.health = "bad"
    print(rex.health)
    # rex.health = ["good", "bad"]
    a = 1
    b = 2
    c = a+b
    # c = a.__add__(b)
    d = str(a)

