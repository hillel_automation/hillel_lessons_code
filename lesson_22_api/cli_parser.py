from argparse import ArgumentParser


def say_hello(name, is_meet):
    if is_meet:
        print(f"Hello {name}")
    else:
        print(f"Bye {name}")


# print(say_hello(human_name))

parser = ArgumentParser()
parser.add_argument("--human_name", help="Argument to get human name", default="Bob")
parser.add_argument("--is_meet", help="Argument to understand if human currently meet somebody", action="store_true")
parser.add_argument("--a", default=1)
parser.add_argument("--b", default=2)
args = parser.parse_args()
print(vars(args))
say_hello(args.human_name, args.is_meet)
print(int(args.a)+int(args.b))
arguments = vars(args)
print(arguments["human_name"])


