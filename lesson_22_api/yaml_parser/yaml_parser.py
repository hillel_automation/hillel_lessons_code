import yaml

# with open("data.yaml", "r") as yaml_file:
#     yaml_data = yaml.safe_load(yaml_file)
#
# print(yaml_data)
#
# yaml_data["doe"] = "Hello"
#
# with open("new_data.yaml", "w") as new_data:
#     yaml.dump(yaml_data, new_data)



data = {
    "name": "Bob",
    "age": 34,
    "addresses": ["address1", "address2"]
}

with open("json_data.yaml", "w") as json_data:
    yaml.dump(data, json_data)