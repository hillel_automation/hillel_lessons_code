# def test_1(people):
#     responce = people.get(1)
#     assert responce.json()["name"] == 'Luke Skywalker'
# def test_2(people):
#     responce = people.get(1)
#     keys = ["name", "height"]
#     print(responce.json())
#     assert all(key in responce.json() for key in keys)
from lesson_22_api.api.entities.people_entity import Person


def test_3(people, luke):
    responce = people.get(1)
    actual_people = Person(
        responce.json()["name"],
        responce.json()["height"],
        responce.json()["mass"],
        responce.json()['hair_color'],
        responce.json()["skin_color"],
        responce.json()['eye_color'],
        responce.json()["birth_year"],
        responce.json()['gender'],
    )
    del responce.json()["edited"]
    assert actual_people == luke


