from requests import get

from lesson_22_api.api.config import config


class People:
    def __init__(self):
        self.url = f"{config['url']}/people"

    def get(self, id):
        return get(f"{self.url}/{id}")
