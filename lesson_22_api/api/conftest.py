import pytest

from lesson_22_api.api.entities.people_entity import Person
from lesson_22_api.api.infrastructure.people import People


@pytest.fixture(scope="session")
def people():
    yield People()


@pytest.fixture(scope="session")
def luke():
    yield Person(name='Luke Skywalker', height='172', mass='77', hair_color='blond', skin_color='fair',
                 eye_color='blue', birth_year='19BBY', gender='male')
