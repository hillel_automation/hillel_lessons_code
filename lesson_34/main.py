import re

ttext = """
netdev@ovs-netdev:
    lookups: hit:50189 missed:949 lost:0  
    flows: 1  
    port 0: ovs-netdev (tap)    
        RX packets:0 errors:0 dropped:0 overruns:0 frame:0    
        TX packets:0 errors:0 dropped:0 aborted:0 carrier:0    
        collisions:0    
        RX bytes:0  TX bytes:0  
    port 1: br-int (tap)    
        RX packets:0 errors:0 dropped:0 overruns:0 frame:0    
        TX packets:0 errors:0 dropped:599 aborted:0 carrier:0    
        collisions:0    
        RX bytes:0  TX bytes:0  
    port 2: vfr123 (tap)    
        RX packets:32 errors:0 dropped:0 overruns:0 frame:0    
        TX packets:619 errors:0 dropped:0 aborted:0 carrier:0    
        collisions:0    
        RX bytes:2344 (2.3 KiB)  TX bytes:65162 (63.6 KiB)  
    port 3: vfr567    
        RX packets:26074 errors:0 dropped:0 overruns:0 frame:0    
        TX packets:25157 errors:0 dropped:0 aborted:0 carrier:0    
        collisions:0    
        RX bytes:2793630 (2.7 MiB)  TX bytes:2415742 (2.3 MiB)  
    port 4: tapb4becd9d-15 (tap), could not retrieve stats (No such device)  
    port 5: qr-bbcef380-07 (tap), could not retrieve stats (No such device)"""

rx = re.findall(f"vfr[\s\S]+?RX packets:(\d+)", ttext)
print(rx)

"""
Get first uniq char of string
Do not use built-in function

Examples
aabbccd -> d
zywzyd -> w
zyzyzy -> None
"""


def unic_char(s):
    new_dict = {}
    for i in s:
        if i in new_dict.keys():
            new_dict[i] = True
        else:
            new_dict[i] = False

    for i in new_dict:
        if new_dict[i] == False:
            return i


def unic_alternative(s):
    new_dict = {}
    for i in s:
        new_dict.update({i: new_dict[i] + 1 if i in new_dict else 1})

    for i in new_dict:
        if new_dict[i] == 1:
            return i


ex_str = "aabbccd"
print(unic_char(ex_str))
print(unic_alternative(ex_str))

"""

[1,2,3,4]
[2,3,4,5]
[4,5,6]
[8,6,7,5]
[6,10,8,9,7]
Expected result: [8,6,7,5]

Input values -> group: [2,3,4,5], channel: 3
"""
groups = [
    [1, 2, 3, 4],
    [2, 3, 4, 5],
    [4, 5, 6],
    [8, 6, 7, 5],
    [6, 10, 8, 9, 7]
]

def my_len(iterable_object):
    return len(iterable_object)

def find_group(group, channel):
    new_list = [i for i in groups if len(i)>=len(group) and channel not in i]
    print(new_list)
    new_list.sort(key=my_len)
    print(new_list)
    return new_list[0]


print(find_group([2,3,4,5], 3))


"""
Multiple table 10x10
3x3

1 2  3   4
2 4  6   8
3 6  9  12
4 8 12  16

"""




