from requests import get
from bs4 import BeautifulSoup

response = get("https://online.ithillel.ua/courses/qa-automation-python")
soup = BeautifulSoup(response.content, "html.parser")
blocks = soup.find_all("div", class_="programme-collapsible_header")
elements = soup.find_all("div", class_="programme-collapsible_container")
themes = dict()

for block in blocks:
    themes[block.contents[0].strip()] = []

blocks_name = list(themes.keys())

counter = 0
for element in elements:
    for block in element.contents[1].contents[1]:
        themes[blocks_name[counter]].append(block.text.strip())
    counter += 1

print(themes)
