from requests import get, post, put, patch, delete

base_url = "https://petstore.swagger.io/v2"

pet = {
  "id": 1,
  "category": {
    "id": 1,
    "name": "string"
  },
  "name": "doggie",
  "photoUrls": [
    "string"
  ],
  "tags": [
    {
      "id": 1,
      "name": "string"
    }
  ],
  "status": "available"
}

user_1 = {
  "id": "9223372036854764699",
  "username": "john",
  "firstName": "John",
  "lastName": "Deer",
  "email": "string@gmail.com",
  "password": "secret",
  "phone": "string",
  "userStatus": 0
}
"""9223372036854764699"""
"""9223372036854764778"""

# response = get(f"{base_url}/user/login", json={"username": "john", "password": "secret"})
response = get(f"{base_url}/user/john")
# response = post(f"{base_url}/user", json=user_1)
# response = put(f"{base_url}/user/john", json=user_1)
# response = patch(f"{base_url}/user/john", json={"firstName": "Bill"})
# response = delete(f"{base_url}/user/john")
print(response.json())

