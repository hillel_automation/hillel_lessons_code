from typing import Tuple

from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By

from lesson_19_selenium.pages.base_page import BasePage
from lesson_19_selenium.pages.product_list import ProductList


class Dashboard(BasePage):

    def __init__(self, driver: Chrome):
        super().__init__(driver)

    def choose_category(self, name: str):
        category_locator: Tuple[By.XPATH, str] = (By.XPATH, f"//span[text()='{name}']/ancestor::div[1]")
        self.click(category_locator)

    def choose_sub_category(self, name):
        sub_category_locator: Tuple[By.XPATH, str] = (By.XPATH, f"//span[text()='{name}']/ancestor::a")
        self.click(sub_category_locator)
        return ProductList(self.driver)
