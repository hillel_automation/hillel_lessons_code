from typing import Tuple

from selenium.webdriver.common.by import By

from lesson_19_selenium.pages.base_page import BasePage
from lesson_19_selenium.pages.product import Product


class ProductList(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self.__first_entry_locator: Tuple[By.XPATH, str] = (By.XPATH, "//div[@data-testid='listing-grid']/div[2]")

    def choose_product(self):
        self.click(self.__first_entry_locator)
        return Product(self.driver)
