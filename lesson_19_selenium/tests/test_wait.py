from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def test_1():

    driver = Chrome("../drivers/chromedriver")
    # driver.implicitly_wait(5)
    web_driver_wait = WebDriverWait(driver, 10)
    driver.get("https://www.google.com/")
    search_locator = "//input[@title='Пошук']"
    first_finded_element = "//ul[@role='listbox']/li[2]"


    search_input = web_driver_wait.until(EC.presence_of_element_located((By.XPATH, search_locator)))
    search_input.send_keys("Hello")

    second_result: WebElement = web_driver_wait.until(EC.presence_of_element_located((By.XPATH, first_finded_element)))
    second_result.click()

    driver.quit()