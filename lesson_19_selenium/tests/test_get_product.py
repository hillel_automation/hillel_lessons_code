def test_get_product(dashboard):
    dashboard.choose_category("Авто")
    product_list = dashboard.choose_sub_category('Легкові автомобілі')
    item_page = product_list.choose_product()
    title = 'OLX.ua - автобазар України - купити авто нове або бу, автомобілі на авторинку'
    assert item_page.get_title(title) == title
