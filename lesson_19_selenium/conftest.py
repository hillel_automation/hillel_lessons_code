import pytest
from selenium.webdriver import Chrome

from lesson_19_selenium.pages.dashboard import Dashboard


@pytest.fixture(scope='session')
def driver():
    driver = Chrome("/home/co/PycharmProjects/hilel_lessons/lesson_19_selenium/drivers/chromedriver")
    driver.maximize_window()

    driver.get("https://www.olx.ua/uk/")
    yield driver
    driver.quit()


@pytest.fixture(scope='session')
def dashboard(driver):
    yield Dashboard(driver)
