class Call:

    def __init__(self):
        self.name = "call"

    def __normal_call(self):
        print("Normal Call")

    def call(self):
        print("First call")
        self.call = self.__normal_call


call = Call()

call.call()
call.call()
call.call()

print(isinstance(call, Call))
print(isinstance(1, int))
print(9//2)











