from bs4 import BeautifulSoup

with open("data.xml") as fp:
    soup = BeautifulSoup(fp, 'html.parser')

# soup.to.string.replace_with("John")
# print(soup.to.string)
# print(soup.prettify())
print(soup.find_all('to'))

for child in soup.parent_internal:
    print(child.string.strip())

