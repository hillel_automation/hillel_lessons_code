x = 'dacb'
y = sorted(x)
h = "-".join(y)
print("-".join(y))

# a = {1, 2, 3}
# print(a[1])
class A:
    def __init__(self):
        self.a = "first"
        self.b = "second"
        self.integer = 1

    def simple(self):
        return 1

a = A()
a.c = "third"
print(a.__dict__)

x = object()
# x.a = "gg"
print(x.__dict__)


