import json

# with open("simple_data.json", "r") as json_data:
#     json_dict = json.load(json_data)
#
# print(json_dict)
# print(type(json_dict))
#
# json_dict["c"] = ["new_element1", "element2"]
#
# with open("simple_data_result.json", "w") as json_data_write:
#     json.dump(json_dict, json_data_write)

with open("data.json", "r") as data:
    json_data = json.load(data)

print(json_data["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["GlossDef"]["GlossSeeAlso"])

