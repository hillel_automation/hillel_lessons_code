class OfficeTable:
    def __init__(self, material, legs, color):
        self.__material = material
        self.__legs = legs
        self.__color = color

    def info(self):
        return f"This chair is {self.__class__.__name__} and has {self.__legs} legs and table material is {self.__material} and table color is {self.__color}"
