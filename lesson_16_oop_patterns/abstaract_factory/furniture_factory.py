from lesson_16_oop_patterns.abstaract_factory.chair_factory import ChairFactory
from lesson_16_oop_patterns.abstaract_factory.table_factory import TableFactory


class FurnitureFactory:

    @staticmethod
    def create_furniture(furniture_type, *args):
        if furniture_type == "chair":
            return ChairFactory(*args)
        elif furniture_type == "table":
            return TableFactory(*args)
        else:
            raise Exception("Incorrect furniture name")

if __name__ == "__main__":
    furniture_style = "home"
    chair_factory = FurnitureFactory.create_furniture("chair", 4, True)
    home_chair = chair_factory.get_chair(furniture_style)
    print(home_chair.info())
    table_factory = FurnitureFactory.create_furniture("table", "wood", 4, "yellow")
    offoce_table = table_factory.get_table(furniture_style)
    print(offoce_table.info())