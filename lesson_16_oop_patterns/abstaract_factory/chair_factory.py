from lesson_16_oop_patterns.abstaract_factory.home_chair import HomeChair
from lesson_16_oop_patterns.abstaract_factory.office_chair import OfficeChair


class ChairFactory:
    def __init__(self, legs, back, has_wheels):
        self.legs = legs
        self.back = back
        self.has_wheels = has_wheels

    def get_chair(self, name, *args):
        if name == "home":
            return HomeChair(self.legs, self.back, *args)
        elif name == "office":
            return OfficeChair(self.legs, self.back, self.has_wheels)
        else:
            raise Exception("Incorrect namr of chair")

