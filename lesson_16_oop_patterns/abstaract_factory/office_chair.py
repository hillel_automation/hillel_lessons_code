class OfficeChair:
    def __init__(self, legs, back):
        self.__legs = legs
        self.__back = back

    def info(self):
        return f"This chair is {self.__class__.__name__} and has {self.__legs} legs and has back is {self.__back}"