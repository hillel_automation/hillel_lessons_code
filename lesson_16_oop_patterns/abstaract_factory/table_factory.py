from lesson_16_oop_patterns.abstaract_factory.home_tabe import HomeTable
from lesson_16_oop_patterns.abstaract_factory.office_table import OfficeTable


class TableFactory:
    def __init__(self, material, legs, color):
        self.__material = material
        self.__legs = legs
        self.__color = color

    def get_table(self, name):
        if name == "home":
            return HomeTable(self.__material, self.__legs, self.__color)
        elif name == "office":
            return OfficeTable(self.__material, self.__legs, self.__color)
        else:
            raise Exception("Incorrect namr of chair")