from lesson_16_oop_patterns.singletone.singletone_db_connection import singletone


@singletone
class DBConnection:

    # instance = DBConnection

    def __init__(self, login: str, password: str):
        self.__login = login
        self.__password = password

    def get_credentials(self):
        return {'login': self.__login, 'password': self.__password}

if __name__ == "__main__":
    connection1 = DBConnection('user1', '12345')
    connection2 = DBConnection('user1', '12345')
    print(connection1)
    print(connection2)



