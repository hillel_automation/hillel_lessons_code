def singletone(_class):
    def wrapper(*args):
        if not hasattr(_class, "instance"):
            setattr(_class, "instance", _class(*args))
        return getattr(_class, "instance")
    return wrapper