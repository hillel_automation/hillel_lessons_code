from lesson_16_oop_patterns.factory_method.drivers.bowser_driver import Browser


class Firefox(Browser):
    _name = "firefox"