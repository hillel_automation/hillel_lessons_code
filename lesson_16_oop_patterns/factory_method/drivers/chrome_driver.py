from lesson_16_oop_patterns.factory_method.drivers.bowser_driver import Browser


class Chrome(Browser):
    _name = "chrome"