from lesson_16_oop_patterns.factory_method.drivers.chrome_driver import Chrome
from lesson_16_oop_patterns.factory_method.drivers.edge_driver import Edge
from lesson_16_oop_patterns.factory_method.drivers.firefox_driver import Firefox


class BrowserFactory:

    @staticmethod
    def get_browser(name):
        if name == "chrome":
            return Chrome()
        elif name == "firefox":
            return Firefox()
        elif name == "edge":
            return Edge()
        else:
            raise Exception("Incorrect browser name")

if __name__ == "__main__":
    chrome = BrowserFactory().get_browser("chrome")
    firefox = BrowserFactory.get_browser("firefox")
    print(chrome._name)
    print(firefox._name)