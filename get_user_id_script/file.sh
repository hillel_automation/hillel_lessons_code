#!/bin/bash
gropus=$(cat /etc/group | egrep ^$1:x | cut -d":" -f4)

if (( ${#gropus} == 0 ));
then
  echo "No such group"
  exit
fi;

groups=$(echo $gropus | sed "s/,/ /g")
for i in $groups
do
  id=$(cat /etc/passwd | egrep ^$i:x | cut -d":" -f3)
  echo "$i $id"
done
