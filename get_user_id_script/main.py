#!/usr/bin/env python3
import sys
from sys import argv


def func(group):
    group_users = ""
    with open("/etc/group", "r") as groups:
        group_lines = groups.readlines()
    for line in group_lines:
        if f"{group}:x" in line:
            group_users = line.split(":")[-1].strip()
    with open("/etc/passwd", "r") as passwd:
        passwd_lines = passwd.readlines()
    user_ids = {}
    if not group_users:
        return "No such group"
    for line in passwd_lines:
        for user in group_users.split(","):
            if f"{user}:x:" in line:
                user_ids.update({user: line.split(":")[2]})
    result_str = ""
    for i in user_ids:
        result_str += f"{i} {user_ids[i]}\n"
    return result_str


if __name__ == "__main__":
    if argv[1]:
        group = argv[1]
    else:
        print("Please input group name")
        sys.exit()
    print(func(group))
