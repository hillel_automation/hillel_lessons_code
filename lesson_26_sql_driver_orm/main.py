import psycopg2


def sql_query(name):
    connection = psycopg2.connect(user='postgres', password='postgres', host='127.0.0.1', port='5432',
                                  database='new_db')
    cursor = connection.cursor()
    cursor.execute(f"select * from people where name='{name}';")
    for row in cursor.fetchall():
        print(row)

    if connection:
        connection.close()


class PeopleRepository:
    def __init__(self):
        self.__connection = psycopg2.connect(user='postgres',
                                             password='postgres',
                                             host='127.0.0.1',
                                             port='5432',
                                             database='new_db')
        self.__cursor = self.__connection.cursor()

    def get_people(self):
        self.__cursor.execute("select * from people;")
        return self.__cursor.fetchall()

    def get_by_name(self, name):
        self.__cursor.execute(f"select * from people where name = '{name}';")
        return self.__cursor.fetchall()

    def update_age_by_name(self, age, name):
        self.__cursor.execute(f"update people set age = {35} where name = '{name}'")
        self.__connection.commit()

    def __del__(self):
        print("Deleting")
        if self.__connection:
            self.__connection.close()


if __name__ == "__main__":
    people = PeopleRepository()
    # print(people.get_people())
    # people.update_age_by_name(35, "John")
    print(people.get_by_name("John"))

