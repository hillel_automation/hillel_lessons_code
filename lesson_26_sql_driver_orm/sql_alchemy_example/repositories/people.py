from sqlalchemy.orm import Session

from lesson_26_sql_driver_orm.sql_alchemy_example.models.people_model import People
from lesson_26_sql_driver_orm.sql_alchemy_example.session import session


class PeopleRepository:
    def __init__(self, session):
        self.__session: Session = session

    def get_by_id(self, people_id):
        print(self.__session.get(People, {'id': people_id}))

    def get_all(self):
        for people in self.__session.query(People).all():
            print(people)

    def get_first(self):
        print(self.__session.query(People).first())

    def insert_people(self, people):
        self.__session.add(people)
        self.__session.commit()

    def get_by_age(self, age):
        print(self.__session.query(People).filter_by(age=f"{age}").first())

    def update_row_by_name(self, name, data: dict):
        self.__session.query(People).filter_by(name=f"{name}").update(data)
        self.__session.commit()



if __name__ == "__main__":
    people = PeopleRepository(session)
    # people.get_by_id(1)
    # people.insert_people(People(name="Bob", age=44, email="bob@gamil.com"))
    # people.update_row_by_name("Bob", {"age": 33, "email": "new_bob@gmail.com"})
    # people.get_all()
    # people.get_by_age(44)
    people.get_first()