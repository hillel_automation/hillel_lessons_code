from sqlalchemy import Integer, Column, VARCHAR
from sqlalchemy.orm import declarative_base

Base = declarative_base()


class People(Base):
    __tablename__ = 'people'

    id: int = Column(Integer, primary_key=True)
    name: str = Column(VARCHAR(50))
    age: int = Column(Integer)
    email: str = Column(VARCHAR(50))

    def __str__(self):
        return f"id: {self.id}  |  name: {self.name} | age: {self.age} | email: {self.email}"
