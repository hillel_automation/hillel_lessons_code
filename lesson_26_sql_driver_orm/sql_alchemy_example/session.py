from sqlalchemy.orm import Session, sessionmaker
from sqlalchemy import create_engine

engine = create_engine("postgresql://postgres:postgres@localhost/new_db")
__session = sessionmaker(engine)
session: Session = __session()
