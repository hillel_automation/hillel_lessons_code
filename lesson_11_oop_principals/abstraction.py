from abc import ABC, abstractmethod


class Employee(ABC):
    def __init__(self, position, salary):
        self.position = position
        self.salary = salary

    @abstractmethod
    def do_work(self):
        pass


class Developer(Employee):
    def __init__(self, position, salary):
        super().__init__(position, salary)

    def do_work(self):
        print("Writing code")

if __name__ == "__main__":
    bob = Developer("Developer", 5000)
    bob.do_work()
