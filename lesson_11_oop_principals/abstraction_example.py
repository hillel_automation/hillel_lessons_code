from abc import ABC, abstractmethod


class Car(ABC):
    def __init__(self, engine, wheels, doors, engine_type):
        self.engine = engine
        self.weels = wheels
        self.doors = doors
        self.engine_type = engine_type

    def move(self):
        print("Move")

    def stop(self):
        print("Stop")

    def trun(self):
        print("Turn")

    @abstractmethod
    def refuel(self):
        pass


class Tesla(Car):
    def __init__(self, engine, wheels, doors, engine_type):
        super().__init__(engine, wheels, doors, engine_type)

    def refuel(self):
        print("Charging")


class Mercedes(Car):
    def __init__(self, engine, wheels, doors, engine_type):
        super().__init__(engine, wheels, doors, engine_type)

    def refuel(self):
        print("Tank up gasoline")


if __name__ == "__main__":
    tesla = Tesla(True, 4, 4, "Electro")
    mercedes = Mercedes(True, 4, 4, "Gasoline")
    tesla.refuel()
    tesla.charging()
    mercedes.refuel()


