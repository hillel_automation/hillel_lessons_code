class Donkey:
    def __init__(self):
        super().__init__()
        self.strength = 10

class Horse:
    def __init__(self):
        super().__init__()
        self.speed = 5

class Mule(Donkey, Horse):
    def __init__(self):
        super().__init__()

if __name__ == "__main__":
    mul = Mule()
    print(mul.strength)
    print(mul.speed)
    print(mul)

