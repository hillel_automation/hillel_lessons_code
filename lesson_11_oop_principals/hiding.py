from abc import ABC, abstractmethod


class Employee(ABC):
    def __init__(self, salary, position):
        """

        Args:
            salary: 
            position:
        """
        self.salary = salary
        self.position = position

    @abstractmethod
    def do_work(self):
        pass


class Engineer(Employee):
    """Engineer class describe engineer abilities"""
    def __init__(self):
        super().__init__(1000, "Engineer")

    def do_work(self):
        print("Do work")

    def __create_framework(self):
        print("Creating new framework")

    def _write_code(self):
        print("Writing new code")

    def deploy(self):
        self._write_code()
        self.__create_framework()


class Developer(Engineer):
    def __init__(self):
        super().__init__()

    def __do_work(self):
        print("Planing code")
        super().do_work()

    def summary(self):
        self.__do_work()

    def do_something(self, param1: str, param2: int):
        print(param1)
        return param2



if __name__ == "__main__":
    engineer = Engineer()
    # engineer.do_work()
    # engineer.deploy()
    dev = Developer()
    # dev.deploy()
    dev.do_work()
    dev.summary()
