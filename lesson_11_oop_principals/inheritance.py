class Human:
    def __init__(self, name, age):
        self.name = name
        self.age = age


class Employee(Human):
    def __init__(self, name, age, position, salary):
        super().__init__(name, age)
        self.position = position
        self.salary = salary

if __name__ == "__main__":
    bob = Employee("Bob", 30, "QA Automation", 100500)
    print(bob.name)
    print(bob.position)
